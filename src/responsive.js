import { css } from 'styled-components';

export const mobile = (props) => {
  return css`
    @media only screen and (max-width: 380px) {
      ${props}
    }
  `;
};
/* 
export const tablet = (props)=>{
    return css`
     @media only screen and (max-width: 380px;){
        ${props}
     }
    `
}
 */

/* 
import { BrowserRouter as Router, Route } from 'react-router-dom

function App() {
  return (
    <div className = "App">
      <Router>
        <Route path='/' element={<Home/>} />
      </Router>
    </div>
  )
})

*/