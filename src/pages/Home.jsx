import React from 'react'
import AppNavBar from '../components/AppNavBar';
import Header from '../components/Header';
import Banner from '../components/Banner';
import Categories from '../components/Categories';
import Products from '../components/Products';
import Newsletter from '../components/Newsletter';
import Footer from '../components/Footer';

const Home = () => {
	return (
		<div>
			<Header />
			<AppNavBar />
			<Banner />
			<Categories />
			<Products />
			<Newsletter />
			<Footer />
		</div>
	)
};

export default Home;