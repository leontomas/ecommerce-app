import 
	React, { 
	useState,
	useEffect,
	useContext,
	createContext 
} from 'react';
// import UserContext from '../userContext'
import { 
	Redirect,
	Navigate,
	useNavigate
 } from 'react-router-dom';
import Swal from "sweetalert2";
import styled from 'styled-components';
import Header from '../components/Header';
import AppNavBar from '../components/AppNavBar';

const Container = styled.div`
	width: 100vw;
	height: 100vh;
	// background: linear-gradient(
	// 	rgba(255,255,255,0.5),
	// 	rgba(255,255,255,0.5)
	// 	), url("https://picsum.photos/1000/1000")center;
	background-color: #1b1b1b;
	background-size: cover;    
	display: flex;
	align-items : center;
	justify-content: center;
	`;
	
const Wrapper = styled.div`
	width: 40%;
	padding: 20px;
	background-color: white;
	`;
	
const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
	`;
	
const Form = styled.form`
	display: flex;
	flex-wrap: wrap;
	`;
	
const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 20px 10px 0px 0px;
	padding: 10px;
	`;
	
const Agreement = styled.span`
	font-size: 12px;
	margin: 20px 0px;
	`;
	
const Button = styled.button`
	width: 40%;
	border: none;
	padding:15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	`;
	
const Contain = styled.div``

// UserContext
const UserContext = createContext();

const UserProvider = UserContext.Provider;

const Register = () => {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [firstName, setFname] = useState('');
	const [lastName, setLname] = useState('');
	const [email, setEmail] = useState("")
	const [confirmEmail, setConfirmEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")
	const [isActive, setIsActive] = useState(false);

	async function registerUser(e){
        e.preventDefault();

        const checkEmailExists = await fetch("http://localhost:4000/api/users/checkEmail", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => data);
        if(!checkEmailExists){
            fetch("http://localhost:4000/api/users/register", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: email,
                    password: confirmPassword
                })
            })
            .then(res => res.json())
            .then(data => {
                Swal.fire({
                    title: "Register Successful!",
                    // icon: "success",
                    text: "You can now Buy items."
                });
                navigate.push("/login");
            });
        }
        else{
            Swal.fire({
                title: "Email Is Already Registered",
                // icon: "error",
                text: "Please select another email."
            });
        }
    }

	useEffect(() => {
        if((
			firstName !== "" &&
			lastName !== "" &&
			email !== "" && 
			confirmEmail !== "" && 
			password !== "" && 
			confirmPassword !== "") && 
				(email === confirmEmail && 
				password === confirmPassword)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password, confirmPassword]);

	return (
		((user.id !== null) || (localStorage.getItem("token") !== null))
        ?
            (user.isAdmin)
            ?
            <Navigate to="/login" />//admin
            :
            <Navigate to="/cart" />//shop
        :

	<Contain>
		<Header />
		<AppNavBar />
		<Container>
			<Wrapper>
				<Title>Create an account</Title>
				<Form>
					<Input placeholder="First name*"/>
					<Input placeholder="Last name*"/>
					<Input placeholder="e-mail*"/>
					<Input placeholder="Confirm your email address*"/>
					<Input placeholder="Password*"/>
					<Input placeholder="Confirm Password*"/>
					<Agreement>
						By creating an account, I consent to the processing of my personal data in accordance with the <b>PRIVACY POLICY</b>
					</Agreement>
					<Button>
					CREATE
					</Button>
				</Form>
			</Wrapper>
		</Container>
	</Contain>
	)
}
export default Register;