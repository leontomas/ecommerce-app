import 
	React, { 
	useState,
	useEffect,
	useContext,
	createContext 
} from 'react';
import { 
	Redirect, 
	Navigate,
	useNavigate 
} from 'react-router-dom';
import Swal from "sweetalert2";
import styled from 'styled-components';
import Header from '../components/Header';
import AppNavBar from '../components/AppNavBar';


const Container = styled.div`
	width: 100vw;
	height: 100vh;
	background: linear-gradient(
		rgba(255,255,255,0.5),
		rgba(255,255,255,0.5)
		), url("https://picsum.photos/1000/1000/?blur")center;
	background-size: cover;
	display: flex;
	align-items : center;
	justify-content: center;
	`;
	
const Wrapper = styled.div`
	width: 25%;
	padding: 20px;
	background-color: white;
	`;
	
const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
	`;
	
const Form = styled.form`
	display: flex;
	// flex-wrap: wrap;
	flex-direction: column;
	`;
	
const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 10px 0px;
	padding: 10px;
	`;
	
const Button = styled.button`
	width: 40%;
	border: none;
	padding:15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;
	margin-bottom: 10px;
	`;

const Link = styled.a`
	margin: 5px 0px;
	font-size: 12px;
	text-decoration: underline;
	cursor: pointer;
`

const Contain = styled.div``
// UserContext
const UserContext = createContext();

const UserProvider = UserContext.Provider;

const Login = () => {
	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false);

	function loginUser(e){
        e.preventDefault();

        fetch("", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);
                Swal.fire({
                    title: "Login Successful!",
                    // icon: "success",
                    text: "You can now shop online."
                });
                if(user.isAdmin){
                    navigate.push("/product");//admin
                }
                else{
                    navigate.push("/cart");//shop
                }
            }
            else{
                Swal.fire({
                    title: "Wrong Email Or Password!",
                    // icon: "error",
                    text: "Please enter your correct email or password.",
                })
            }
        });
		setEmail('');
		setPassword('');

		console.log(`${email} has been verified! Welcome back!`);
    }

	const retrieveUserDetails = (token) => {
        fetch("https://ancient-temple-55465.herokuapp.com/api/users/details", {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id:data._id,
                isAdmin:data.isAdmin,
                email: email
            });
        });
    }

	useEffect(() => {
        if((email !== "" && password !== "")){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [email, password]);

	return (

		((user.id !== null) || (localStorage.getItem("token") !== null))
        ?
            (user.isAdmin)
            ?
            <Navigate to="/cart" />//admin
            :
            <Navigate to="/product" />//shop
        :

	<Contain>
		<Header />
		<AppNavBar />
		<Container>
			<Wrapper>
				<Title>SIGN IN</Title>
				<Form>
					<Input placeholder="username"/>
					<Input placeholder="password"/>
					<Button>LOGIN</Button>
					<Link>PASSWORD RESET?</Link>
					<Link>CREATE A NEW ACCOUNT</Link>
				</Form>
			</Wrapper>
		</Container>
	</Contain>
	)
}

export default Login;
