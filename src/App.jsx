import './App.css';
import { 
  Link,
  Route,
  Routes,
  BrowserRouter as Router
 } from 'react-router-dom';
import Cart from './pages/Cart';
import Home from './pages/Home';
import Login from './pages/Login';
import Product from './pages/Product';
import Register from './pages/Register';
import ErrorPage from './pages/ErrorPage';
import ProductList from './pages/ProductList';

const App = () => {
	return (
		<Router>
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/cart" element={<Cart />} />
				<Route path="/login" element={<Login />} />
				<Route path="/product" element={<Product />} />
				<Route path="/register" element={<Register />} />
				<Route path="/productlist" element={<ProductList />} />
				<Route path="*" element={<ErrorPage/>}/>
			</Routes>
		</Router>
	)
};

/* 
//  Objectives:
//  - Create a full-stack e-commerce web app using React and the Express API made during Capstone 2.
//  - All required features must be present
//  - App will be hosted on Vercel

// Requirements:
//  - A Registration Page with proper form validation (all fields must be filled and passwords must match) that must redirect the user to the login page once successfully submitted.

//  - A Login Page that must redirect the user to the either the home page or the products catalog once successfully authenticated

//  - A Cart View page with the following features:
//    - Show all items the user has added to their cart (and their quantities)
//    - Change product quantities
//    - Remove products from cart
//    - Subtotal for each item
//    - Total price for all items
//    - A working checkout button/functionality.
//    - When the user checks their cart out, redirect them to either the homepage or the Order History page

//  - An Order History page where the currently logged-in user can see all records of their own previously-placed orders

//  - An Admin Panel/Dashboard with the following features:
//    - Retrieve list of all products (available or unavailable)
//    - Create new product
//    - Update product information
//    - Deactivate/reactivate product

// Other requirements:
//  - A fully-functioning Navbar with proper dynamic rendering (Register/Login links for users not logged in, Logout link for users who are, etc)
//  - App must be single-page and utilize proper routing (no navigating to another page/reloading)
//  - Registration/Login pages must be inaccessible to users who are logged-in
//  - Apart from users who are not logged-in, Admin must not be able to add products to their cart
//  - Do not create a website other than the required e-commerce app
//  - Do not use templates found in other sites or existing premade NPM packages that replicate a required feature

// Stretch goals:
//  - Full responsiveness across mobile/tablet/desktop screen sizes
//  - Product images
//  - A hot products/featured products section
//  - Admin feature to retrieve a list of all orders made by all users 
*/

/* 
Sample of Routing
import { BrowserRouter as Router, Route } from 'react-router-dom

function App() {
  return (
	<div className = "App">
	  <Router>
		<Route path='/' element={<Home/>} />
	  </Router>
	</div>
  )
})

*/

export default App;